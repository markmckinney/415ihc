# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1513888711.8037298
_enable_loop = True
_template_filename = 'C:/Users/josep/415ihc/ihc/homepage/templates/results.html'
_template_uri = 'results.html'
_source_encoding = 'utf-8'
import django_mako_plus
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        hours = context.get('hours', UNDEFINED)
        minutes = context.get('minutes', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        hours = context.get('hours', UNDEFINED)
        minutes = context.get('minutes', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\r\n        <!-- Page Content -->\r\n        <div id="page-wrapper">\r\n            <div class="container-fluid">\r\n                <div class="row">\r\n                    <div class="col-lg-12">\r\n                        <h1 class="page-header">Prediction Results</h1>\r\n                    </div>\r\n                    <!-- /.col-lg-12 -->\r\n                          <div class="container-fluid">\r\n\r\n\r\n\r\n                                <h4  class="text-muted">We predict that this job will take</h4>\r\n\r\n\r\n                              <h1 style="color: red; font-weight: bold;">')
        __M_writer(str(hours))
        __M_writer(' hours and ')
        __M_writer(str(minutes))
        __M_writer(' minutes</h1>\r\n                              <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>\r\n                              <br><br><br><br><br><br><br><br><br>\r\n                          </div>\r\n                </div>\r\n                <!-- /.row -->\r\n            </div>\r\n            <!-- /.container-fluid -->\r\n        </div>\r\n        <!-- /#page-wrapper -->\r\n\r\n  ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/josep/415ihc/ihc/homepage/templates/results.html", "uri": "results.html", "source_encoding": "utf-8", "line_map": {"28": 0, "37": 1, "38": 2, "43": 33, "49": 4, "57": 4, "58": 22, "59": 22, "60": 22, "61": 22, "67": 61}}
__M_END_METADATA
"""
