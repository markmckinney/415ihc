# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1513804003.696026
_enable_loop = True
_template_filename = 'C:/Users/markj/415ihc/ihc/homepage/templates/display_form.html'
_template_uri = 'display_form.html'
_source_encoding = 'utf-8'
import django_mako_plus
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        message = context.get('message', UNDEFINED)
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)


        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        message = context.get('message', UNDEFINED)
        form = context.get('form', UNDEFINED)
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\r\n        <!-- Page Content -->\r\n        <div id="page-wrapper">\r\n            <div class="container-fluid">\r\n                <div class="row">\r\n                    <div class="col-lg-12">\r\n                        <h1 class="page-header">')
        __M_writer(str(message))
        __M_writer('</h1>\r\n                        <div class="content">\r\n                          <p class="text-muted">\r\n\r\n                            The following application receives the input from the following form fields to create a\r\n                            prediction for how long a task will take based on the an algorithm developed in Microsoft Azure ML studio\r\n                            (using the PCU patient data).\r\n\r\n                          </p>\r\n                          <div class="row">\r\n\r\n                              <div class="col-lg-6">\r\n\r\n                                <div class = \'form\'>\r\n                                ')
        __M_writer(str(form))
        __M_writer('\r\n                                </div>\r\n                                <br><br><br><br><br><br>\r\n\r\n                              </div>\r\n                          </div><br><br><br><br><br><br><br><br><br><br><br>\r\n                        </div>\r\n                    </div>\r\n                    <!-- /.col-lg-12 -->\r\n                </div>\r\n                <!-- /.row -->\r\n            </div>\r\n            <!-- /.container-fluid -->\r\n        </div>\r\n        <!-- /#page-wrapper -->\r\n\r\n  ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
<<<<<<< HEAD
{"filename": "C:/Users/markj/415ihc/ihc/homepage/templates/display_form.html", "uri": "display_form.html", "source_encoding": "utf-8", "line_map": {"28": 0, "37": 1, "42": 41, "48": 3, "56": 3, "57": 11, "58": 11, "59": 25, "60": 25, "66": 60}}
=======
{"filename": "C:/Users/josep/415ihc/ihc/homepage/templates/display_form.html", "uri": "display_form.html", "source_encoding": "utf-8", "line_map": {"28": 0, "37": 1, "42": 27, "48": 3, "56": 3, "57": 11, "58": 11, "59": 15, "60": 15, "66": 60}}
>>>>>>> 4d512adbbe755201e186b0e3ef6afa6557c4c283
__M_END_METADATA
"""
