# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1513803701.2579763
_enable_loop = True
_template_filename = 'C:/Users/markj/415ihc/ihc/homepage/templates/tableau1.html'
_template_uri = 'tableau1.html'
_source_encoding = 'utf-8'
import django_mako_plus
_exports = ['content']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'base.htm', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n\r\n')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n\r\n\r\n        <!-- Page Content -->\r\n        <div id="page-wrapper">\r\n            <div class="container-fluid">\r\n                <div class="row">\r\n                    <div class="col-lg-12">\r\n                        <h1 class="page-header">Tableau Dashboard 1</h1>\r\n                    </div>\r\n                    <!-- /.col-lg-12 -->\r\n                    <div class=\'tableauPlaceholder\' id=\'viz1513728728916\' style=\'position: relative\'>\r\n                      <noscript>\r\n                        <a href=\'#\'>\r\n                          <img alt=\'Prediction Trend \' src=\'https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;IH&#47;IHC-NurseAcuity&#47;PredictionTrend&#47;1_rss.png\' style=\'border: none\' />\r\n                        </a>\r\n                      </noscript>\r\n                      <object class=\'tableauViz\'  style=\'display:none;\'><param name=\'host_url\' value=\'https%3A%2F%2Fpublic.tableau.com%2F\' />\r\n                        <param name=\'embed_code_version\' value=\'3\' />\r\n                        <param name=\'site_root\' value=\'\' />\r\n                        <param name=\'name\' value=\'IHC-NurseAcuity&#47;PredictionTrend\' />\r\n                        <param name=\'tabs\' value=\'no\' />\r\n                        <param name=\'toolbar\' value=\'yes\' />\r\n                        <param name=\'static_image\' value=\'https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;IH&#47;IHC-NurseAcuity&#47;PredictionTrend&#47;1.png\' />\r\n                         <param name=\'animate_transition\' value=\'yes\' />\r\n                         <param name=\'display_static_image\' value=\'yes\' />\r\n                         <param name=\'display_spinner\' value=\'yes\' />\r\n                         <param name=\'display_overlay\' value=\'yes\' />\r\n                         <param name=\'display_count\' value=\'yes\' />\r\n                         <param name=\'filter\' value=\'publish=yes\' />\r\n                       </object></div>\r\n                       <script type=\'text/javascript\'>\r\n                        var divElement = document.getElementById(\'viz1513728728916\');\r\n                             var vizElement = divElement.getElementsByTagName(\'object\')[0];\r\n                                    vizElement.style.width=\'100%\';vizElement.style.height=(divElement.offsetWidth*0.75)+\'px\';\r\n                                      var scriptElement = document.createElement(\'script\');\r\n                                       scriptElement.src = \'https://public.tableau.com/javascripts/api/viz_v1.js\';\r\n                                             vizElement.parentNode.insertBefore(scriptElement, vizElement);\r\n                        </script>\r\n                </div>\r\n                <!-- /.row -->\r\n            </div>\r\n            <!-- /.container-fluid -->\r\n        </div>\r\n        <!-- /#page-wrapper -->\r\n\r\n  ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/markj/415ihc/ihc/homepage/templates/tableau1.html", "uri": "tableau1.html", "source_encoding": "utf-8", "line_map": {"28": 0, "35": 1, "40": 49, "46": 3, "52": 3, "58": 52}}
__M_END_METADATA
"""
