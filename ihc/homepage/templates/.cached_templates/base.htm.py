# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1513904948.1649966
_enable_loop = True
_template_filename = 'C:/Users/josep/415ihc/ihc/homepage/templates/base.htm'
_template_uri = 'base.htm'
_source_encoding = 'utf-8'
import django_mako_plus
_exports = ['head_title', 'content']


from datetime import datetime 

def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        request = context.get('request', UNDEFINED)
        STATIC_URL = context.get('STATIC_URL', UNDEFINED)
        self = context.get('self', UNDEFINED)
        def head_title():
            return render_head_title(context._locals(__M_locals))
        def content():
            return render_content(context._locals(__M_locals))
        __M_writer = context.writer()
        __M_writer('\r\n')
        __M_writer('\r\n<!DOCTYPE html>\r\n<html>\r\n    <meta charset="UTF-8">\r\n\r\n    <head>\r\n\r\n\r\n        <title>')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'head_title'):
            context['self'].head_title(**pageargs)
        

        __M_writer('</title>\r\n\r\n        <!-- Bootstrap Core CSS -->\r\n        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >\r\n\r\n        <!-- Custom CSS -->\r\n        <link href="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/styles/styles.css" rel="stylesheet">\r\n\r\n        <!-- menu CSS -->\r\n        <link href="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/styles/metisMenu.css" rel="stylesheet">\r\n\r\n        <!-- Custom Fonts -->\r\n        <link href="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/styles/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">\r\n\r\n        <!-- jQuery -->\r\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/scripts/jquery.js"></script>\r\n\r\n        <!-- menu js -->\r\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/scripts/metisMenu.js"></script>\r\n\r\n        <!-- table to csv js -->\r\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/scripts/table_to_csv.js"></script>\r\n\r\n        <!-- Bootstrap Core JavaScript -->\r\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/styles/bootstrap/js/bootstrap.js"></script>\r\n\r\n        <!-- Custom Theme JavaScript -->\r\n        <script src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/scripts/ihc.js"></script>\r\n\r\n    </head>\r\n\r\n    <body>\r\n\r\n        <div id="wrapper">\r\n<!--\r\n                  <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'portfolio' else '' ))
        __M_writer(' nav-item px-lg-4">\r\n                    <a class="nav-link text-uppercase text-expanded" href="/portfolio">Portfolio</a>\r\n                  </li> -->\r\n\r\n        <!-- DMW Navigation ASDAF\'AFKL;GFSKDFL;GFMGL;\'FGKMSL;\'GKMFL;SGKS;\'KSGFL;\'KGS\'L;GK\'GL;KFSDGL;FDSKGLS;\'KS\'GKS\';G -->\r\n\r\n            <!-- Navigation -->\r\n            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">\r\n                <div class="navbar-header">\r\n                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">\r\n                        <span class="sr-only">Toggle navigation</span>\r\n                        <span class="icon-bar"></span>\r\n                        <span class="icon-bar"></span>\r\n                        <span class="icon-bar"></span>\r\n                    </button>\r\n                    <a class="navbar-brand" href="/index">\r\n                      <!-- <img src="')
        __M_writer(str( STATIC_URL ))
        __M_writer('homepage/media/logo.png"> -->Intermountain Healthcare\r\n                    </a>\r\n                </div>\r\n                <!-- /.navbar-header -->\r\n\r\n                <div class="navbar-default sidebar" role="navigation">\r\n                    <div class="sidebar-nav navbar-collapse">\r\n                        <ul class="nav" id="side-menu">\r\n                          <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'index' else '' ))
        __M_writer('">\r\n                            <a href="/index"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>\r\n                          </li>\r\n                          <li class="')
        __M_writer(str( 'active' if request.dmp_router_page == 'data' else '' ))
        __M_writer('">\r\n                            <a href="/data"><i class="fa fa-table fa-fw"></i> Raw Data</a>\r\n                          </li>\r\n\r\n\r\n                            <li>\r\n                                <a href="#"><i class="fa fa-windows fa-fw"></i> Azure<span class="fa arrow"></span></a>\r\n                                <ul class="nav nav-second-level">\r\n                                  <li>\r\n                                      <a href="/makeprediction"><i class="fa fa-edit fa-fw"></i>Make Prediction</a>\r\n                                  </li>\r\n                                </ul>\r\n                                <!-- /.nav-second-level -->\r\n                            </li>\r\n                        </ul>\r\n                    </div>\r\n                    <!-- /.sidebar-collapse -->\r\n                </div>\r\n                <!-- /.navbar-static-side -->\r\n            </nav>\r\n\r\n        <main>\r\n            ')
        if 'parent' not in context._data or not hasattr(context._data['parent'], 'content'):
            context['self'].content(**pageargs)
        

        __M_writer('\r\n\r\n        </main>\r\n      </div>\r\n      <!-- /#wrapper -->\r\n\r\n\r\n')
        __M_writer('\r\n    <!--')
        __M_writer(str(django_mako_plus.link_js(self)))
        __M_writer('-->\r\n\r\n  </body>\r\n</html>\r\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_head_title(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def head_title():
            return render_head_title(context)
        __M_writer = context.writer()
        __M_writer('Intermountain Healthcare')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_content(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        def content():
            return render_content(context)
        __M_writer = context.writer()
        __M_writer('\r\n                Site content goes here in sub-templates.\r\n            ')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "C:/Users/josep/415ihc/ihc/homepage/templates/base.htm", "uri": "base.htm", "source_encoding": "utf-8", "line_map": {"17": 3, "19": 0, "31": 2, "32": 3, "37": 11, "38": 17, "39": 17, "40": 20, "41": 20, "42": 23, "43": 23, "44": 26, "45": 26, "46": 29, "47": 29, "48": 32, "49": 32, "50": 35, "51": 35, "52": 38, "53": 38, "54": 46, "55": 46, "56": 62, "57": 62, "58": 70, "59": 70, "60": 73, "61": 73, "66": 97, "67": 105, "68": 106, "69": 106, "75": 11, "81": 11, "87": 95, "93": 95, "99": 93}}
__M_END_METADATA
"""
