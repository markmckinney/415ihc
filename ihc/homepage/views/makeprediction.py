from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function,render_template
from homepage import models
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string
import urllib.request
import json

@view_function
def process_request(request):
    message = 'Make Prediction'
    form = new_device(request)
    if form.is_valid():
        data =  {"Inputs":
                    {"input1":
                        {"ColumnNames": ["Clarvia Hours", "RN Estimated Hours",
                                         "Diagnosis difficulty level.  1=easiest through 3=hardest",
                                         "Abnormal Labs (Binary)",
                                         ],
                         "Values": [ [ str(form.cleaned_data.get('Clarvia Hours')),
                                       "0",
                                       str(form.cleaned_data.get('Diagnosis Difficulty Level')),
                                           str(form.cleaned_data.get('Abnormal Labs')),
                                           ]
                                    ]
                        },
                    },
                "GlobalParameters": {
                    }
        }
        body = str.encode(json.dumps(data))
        url = 'https://ussouthcentral.services.azureml.net/workspaces/a25cb7a0dbfc4f7b9c1f22b17ac47915/services/519bd6c2fa0c4f7a8056098f282df4bb/execute?api-version=2.0&details=true'
        api_key = 'uyLrJ7sQ37QKH0zXOAquguqgKr8jn2LVEHpmsuOdBaBMbrwLSKv8aCEnJ+LPvZHZo9s2zCjRYHZlYmgl1OMohQ==' # Replace this with the API key for the web service
        headers = {'Content-Type':'application/json', 'Authorization':('Bearer '+ api_key)}
        req = urllib.request.Request(url, body, headers)
        response = urllib.request.urlopen(req)
        result = response.read()
        result = json.loads(result)
        prediction = result["Results"]["output1"]["value"]["Values"][0][0]
        prediction = round(float(prediction),2)
        print (prediction)
        minutes = prediction - int(prediction)
        minutes = int(minutes*60)
        hours = int(prediction)

        return HttpResponse(render_template(request, "homepage", 'results.html',{"minutes":minutes, "hours":hours}))
    return HttpResponse(render_template(request,"homepage", 'display_form.html',{"form":form,"message":message}))

class new_device(FormMixIn, forms.Form):
    def init(self):

        self.fields['Clarvia Hours'] = forms.DecimalField(label="Clarvia Hours")
        self.fields['Diagnosis Difficulty Level'] = forms.ChoiceField(
                    label="Diagnosis Difficulty Level",
                    choices = [(1,"1-Easy"), (2,"2-Medium"), (3,"3-Hard")]
                    )
        self.fields['Abnormal Labs'] = forms.ChoiceField(
                    label="Abnormal Labs",
                    choices = [(1,"Yes"), (0,"No")]
                    )
