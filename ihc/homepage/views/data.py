from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function,render_template
from homepage import models
from formlib.form import FormMixIn
from .. import dmp_render, dmp_render_to_string
import urllib.request
import json

@view_function
def process_request(request):
    return HttpResponse(render_template(request,"homepage", 'data.html',{}))
